# Commands for Hubspot Migration

Export Contacts from hubspot API declared inside settings

```php public/index.php /export-contacts GET > /path/to/file.csv```

Import Contacts CSV to hubspot and save details to database

```php public/index.php /import-contacts GET path=/path/to/file```

Import Companies CSV to hubspot and save details to database

```php public/index.php /import-companies GET path=/path/to/file```

Associate Company to Contacts using the database that was imported earlier

```php public/index.php /associate-companies-contacts GET x=x```

Copy all Contact properties from one hubspot Api to another

```php public/index.php /transfer-contact-property GET "fromapi=8935d20f-e704-4af6-8f1f-cc202c221180&toapi=8f4c8efd-4449-42e6-a22a-e38191696be7"```

Copy all Company properties from one hubspot Api to another

```php public/index.php /transfer-company-property GET "fromapi=8935d20f-e704-4af6-8f1f-cc202c221180&toapi=8f4c8efd-4449-42e6-a22a-e38191696be7"```

# Amazon AWIS / Alexa Top/Shopping sites

Get top 30,000 sites from Top/Shopping. Be sure to change the AWS settings inside `Action/Export.php` on `alexa()` method

```php public/index.php /transfer-contact-property GET count=30000 > /path/to/file.csv```