<?php

use App\Action\Export;
use App\Models\DealsModel;
use App\Action\Import;
use App\Models\HttpClient;
use App\Models\AccountsModel;
use App\Models\HubspotLog;
use App\Models\CompaniesModel;
use App\Models\HubSpotClient;
use App\Models\PoundfitModel;
use GuzzleHttp\Client;


$container = $app->getContainer();

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings');
    $logger = new Monolog\Logger($settings['logger']['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['logger']['path'], Monolog\Logger::DEBUG));

    return $logger;
};

// Database
$container['pdo'] = function ($c) {
    $settings = $c->get('settings')['pdo'];

    $pdo = new PDO($settings['dsn'], $settings['username'], $settings['password']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $pdo;
};

// HTTP Client


$container[App\Models\HttpClient::class] = function ($c) {
    $hapiKey = $c->get('settings')['hapiKey'];
    return new HttpClient($c->get('logger'),
                    new GuzzleHttp\Client(),
                    $hapiKey);
};

// Guzzle Hubspot Client
$container[App\Models\HubSpotClient::class] = function ($c) {
    $hapiKey = $c->get('settings')['hapiKey'];
    return new HubSpotClient($c->get('logger'),
                    new GuzzleHttp\Client(),
                    $hapiKey,
                    $c->get('App\Models\HubspotLog'));
};


// Test
$container[App\Models\DealsModel::class] = function ($c) {
    return new DealsModel($c->get('logger'), $c->get('pdo'));
};

$container[App\Models\AccountsModel::class] = function ($c) {
    return new AccountsModel($c->get('logger'), $c->get('pdo'));
};

$container[App\Models\CompaniesModel::class] = function ($c) {
    return new CompaniesModel($c->get('logger'), $c->get('pdo'));
};

$container[App\Models\PoundfitModel::class] = function ($c) {
    return new PoundfitModel($c->get('logger'), $c->get('pdo'));
};

$container[App\Models\HubspotLog::class] = function ($c) {
    return new HubspotLog($c->get('pdo'));
};

$container[App\Action\Export::class] = function ($c) {
    return new Export($c->get('logger'),
                         $c->get('App\Models\DealsModel'),
                         $c->get('App\Models\HubSpotClient'),
                         $c->get('App\Models\PoundfitModel')
                         );
};

$container[App\Action\Import::class] = function ($c) {
    return new Import($c->get('logger'),
                         $c->get('App\Models\AccountsModel'),
                         $c->get('App\Models\DealsModel'),
                         $c->get('App\Models\CompaniesModel'),
                         $c->get('App\Models\HubSpotClient'));
};