<?php

namespace App\Models;

// use App\Models\HubSpotClient;
use Psr\Log\LoggerInterface;
use PDO;

/**
 * Class DealsFactory.
 */
class CompaniesModel
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \PDO
     */
    private $pdo;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     * @param \PDO                     $pdo
     */
    public function __construct(LoggerInterface $logger, PDO $pdo)
    {
        $this->logger = $logger;
        $this->pdo = $pdo;
    }

    public function saveRow($data)
    {
        $valuesText = []; 
        $valuesText[] = "NULL";
        $values = []; 
        $columns = []; 
        $columns[] = "`id`";

        $dateColumns = ['systemmodstamp'];
        foreach($data as $key => $row)
        {

            $columns[] = "`{$key}`";
            $valuesText[] = "?";
            // convert date to microtimes
            if((strpos( strtolower($key), 'date') > 0 ||
                in_array(strtolower($key), $dateColumns)) &&
                !empty($row))
            {
                     
                $date = new \DateTime( $row );

                $date->setTimezone( timezone_open( 'UTC' ) );
                $date->modify( 'midnight' );

                $values[] = $date->getTimestamp() * 1000;
                continue;
            }
            $values[] = $row;
        }

        // build the sql
        $sql = "INSERT INTO `companies` ( ";
        $sql .= implode(", \n", $columns);
        $sql .= " ) ";
        $sql .= "VALUES (";
        $sql .= implode(", ", $valuesText);        
        $sql .= ")\n"; 

        try {

            $statement = $this->pdo->prepare($sql);
            $statement->execute($values);
        } catch(PDOException $e) {
            echo $e->getMessage();
            exit;
        }

        return $statement;

    }

    public function associateContactsFromDb($hubspotClient = NULL)
    {
        $sql = 'SELECT 
                    `ct`.`vid`,
                    `ct`.`email`,
                    `cm`.`companyId`,
                    `cm`.`name` as `companyName`
                FROM `contacts` `ct`
                JOIN `companies` `cm`
                ON (`ct`.`salesforceaccountid` = `cm`.`salesforceaccountid`)
                ORDER BY `companyName`
                ';


        $statement = $this->pdo->prepare($sql);
        $statement->execute();

        if($statement->rowCount() == 0){
            return FALSE;
        }

        return $statement->fetchAll(PDO::FETCH_ASSOC);

                
    }





}
