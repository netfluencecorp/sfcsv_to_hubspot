<?php

namespace App\Models\Engagements;


class Meeting implements CSVInterface
{
    private $csvArray;
    use AssociationTrait;
    use EngagementTrait;

    public function __construct()
    {
        $this->csvArray[] = $this->getCSVHeader();
    }

    public function getCSVHeader()
    {
        return ['engagementid', 
                'createdAt',
                'lastUpdated', 
                'createdBy', 
                'modifiedBy', 
                'owner',
                'timestamp',
                'contactIds',
                'companyIds',
                'dealIds',
                'ownerIds',

                'startTime',
                'endTime',
                'title',
                'body',
                ];
    }



    public function flatten($engagement)
    {

        $data = []; 

        $this->extractObjectInfo($data, $engagement['engagement']);
        $this->extractAssociations($data, $engagement['associations']);
        // exit;
        $data[] = (isset($engagement['metadata']['startTime']))?  Date('Y-m-d H:i:s', ((int)($engagement['metadata']['startTime']/1000))) : '';
        $data[] = (isset($engagement['metadata']['endTime']))?  Date('Y-m-d H:i:s', ((int)($engagement['metadata']['endTime']/1000))) : '';
        $data[] = (isset($engagement['metadata']['title']))? $engagement['metadata']['title'] : '';
        $data[] = (isset($engagement['metadata']['body']))? $engagement['metadata']['body'] : '';
            
        $this->csvArray[] = $data;
    }

    public function getData()
    {
        return $this->csvArray;
        // return array_merge($this->getCSVHeader,$this->csvArray);
    }

    public function __toString() {
        return 'meeting';
    }





}