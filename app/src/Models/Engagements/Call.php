<?php

namespace App\Models\Engagements;


class Call implements CSVInterface
{
    private $csvArray;
    use AssociationTrait;
    use EngagementTrait;

    public function __construct()
    {
        $this->csvArray[] = $this->getCSVHeader();
    }

    public function getCSVHeader()
    {

                $data[] = (isset($engagement['id']))? $engagement['id'] : '';
        $data[] = (isset($engagement['createdAt']))?  Date('Y-m-d H:i:s', ((int)($engagement['createdAt']/1000))) : '';
        $data[] = (isset($engagement['lastUpdated']))?  Date('Y-m-d H:i:s', ((int)($engagement['lastUpdated']/1000))) : '';
        $data[] = (isset($engagement['createdBy']))? $engagement['createdBy'] : '';
        $data[] = (isset($engagement['modifiedBy']))? $engagement['modifiedBy'] : '';
        $data[] = (isset($engagement['ownerId']))? $engagement['ownerId'] : '';
        $data[] = (isset($engagement['timestamp']))?  Date('Y-m-d H:i:s', ((int)($engagement['timestamp']/1000))) : '';
        
        return ['engagementid', 
                'createdAt',
                'lastUpdated', 
                'createdBy', 
                'modifiedBy', 
                'owner',
                'timestamp',

                'contactIds',
                'companyIds',
                'dealIds',
                'ownerIds',

                'toNumber',
                'fromNumber',
                'status',
                'externalId',
                'durationMilliseconds',
                'externalAccountId',
                'recordingUrl',
                'body',
                ];
    }



    public function flatten($engagement)
    {

        $data = []; 

        $this->extractObjectInfo($data, $engagement['engagement']);
        $this->extractAssociations($data, $engagement['associations']);

        $data[] = (isset($engagement['metadata']['toNumber']))? $engagement['metadata']['toNumber'] : '';
        $data[] = (isset($engagement['metadata']['fromNumber']))? $engagement['metadata']['fromNumber'] : '';
        $data[] = (isset($engagement['metadata']['status']))? $engagement['metadata']['status'] : '';
        $data[] = (isset($engagement['metadata']['externalId']))? $engagement['metadata']['externalId'] : '';
        $data[] = (isset($engagement['metadata']['durationMilliseconds']))? $engagement['metadata']['durationMilliseconds'] : '';
        $data[] = (isset($engagement['metadata']['externalAccountId']))? $engagement['metadata']['externalAccountId'] : '';
        $data[] = (isset($engagement['metadata']['recordingUrl']))? $engagement['metadata']['recordingUrl'] : '';
        $data[] = (isset($engagement['metadata']['body']))? $engagement['metadata']['body'] : '';

        $this->csvArray[] = $data;
    }

    public function getData()
    {
        return $this->csvArray;
        // return array_merge($this->getCSVHeader,$this->csvArray);
    }


    public function __toString() {
        return 'call';
    }




}