<?php

namespace App\Models\Engagements;


class Email implements CSVInterface
{
    private $csvArray;
    // private $name = 'email';
    use AssociationTrait;
    use EngagementTrait;

    public function __construct()
    {
        $this->csvArray[] = $this->getCSVHeader();
    }

    public function getCSVHeader()
    {
        return ['engagementid', 
                'createdAt',
                'lastUpdated', 
                'createdBy', 
                'modifiedBy', 
                'owner',
                'timestamp',
                'contactIds',
                'companyIds',
                'dealIds',
                'ownerIds',

                'from_email',
                'from_fname',
                'from_lname',
                'to',
                'subject',
                'text',
                'trackerKey',
                ];
    }



    public function flatten($engagement)
    {

        $data = []; 

        $this->extractObjectInfo($data, $engagement['engagement']);
        $this->extractAssociations($data, $engagement['associations']);
        // exit;
        $data[] = (isset($engagement['metadata']['from']['email']))? $engagement['metadata']['from']['email'] : '';
        $data[] = (isset($engagement['metadata']['from']['firstName']))? $engagement['metadata']['from']['firstName'] : '';
        $data[] = (isset($engagement['metadata']['from']['lastName']))? $engagement['metadata']['from']['lastName'] : '';

        // collect all emails in to field
        if(isset($engagement['metadata']['to'])){
            $emailCollection = [];
            foreach($engagement['metadata']['to'] as $to){
                $emailCollection[] = $to['email'];
            }
            $data[] = implode(':', $emailCollection);
        }else{
            $data[] = '';
        }

        $data[] = (isset($engagement['metadata']['subject']))? $engagement['metadata']['subject'] : '';
        $data[] = (isset($engagement['metadata']['text']))? $engagement['metadata']['text'] : '';
        $data[] = (isset($engagement['metadata']['trackerKey']))? $engagement['metadata']['trackerKey'] : '';
            
        $this->csvArray[] = $data;
    }

    public function getData()
    {
        return $this->csvArray;
    }

    public function __toString() {
        return 'email';
    }


}