<?php

namespace App\Models\Engagements;


Trait EngagementTrait {

    public function extractObjectInfo(&$data, $engagement)
    {

        $data[] = (isset($engagement['id']))? $engagement['id'] : '';
        $data[] = (isset($engagement['createdAt']))?  Date('Y-m-d H:i:s', ((int)($engagement['createdAt']/1000))) : '';
        $data[] = (isset($engagement['lastUpdated']))?  Date('Y-m-d H:i:s', ((int)($engagement['lastUpdated']/1000))) : '';
        $data[] = (isset($engagement['createdBy']))? $engagement['createdBy'] : '';
        $data[] = (isset($engagement['modifiedBy']))? $engagement['modifiedBy'] : '';
        $data[] = (isset($engagement['ownerId']))? $engagement['ownerId'] : '';
        $data[] = (isset($engagement['timestamp']))?  Date('Y-m-d H:i:s', ((int)($engagement['timestamp']/1000))) : '';
        
    }
}