<?php
namespace App\Models\Engagements;


Interface CSVInterface{

    public function getCSVHeader();
    public function flatten($engagement);
    public function getData();

}