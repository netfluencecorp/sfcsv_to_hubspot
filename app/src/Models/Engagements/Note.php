<?php

namespace App\Models\Engagements;


class Note implements CSVInterface
{
    private $csvArray;
    use AssociationTrait;
    use EngagementTrait;

    public function __construct()
    {
        $this->csvArray[] = $this->getCSVHeader();
    }

    public function getCSVHeader()
    {
        return ['engagementid', 
                'createdAt',
                'lastUpdated', 
                'createdBy', 
                'modifiedBy', 
                'owner',
                'timestamp',
                'contactIds',
                'companyIds',
                'dealIds',
                'ownerIds',

                'body',
                ];
    }



    public function flatten($engagement)
    {

        $data = []; 

        $this->extractObjectInfo($data, $engagement['engagement']);
        $this->extractAssociations($data, $engagement['associations']);
        $data[] = (isset($engagement['metadata']['body']))? $engagement['metadata']['body'] : '';
            
        $this->csvArray[] = $data;
    }

    public function getData()
    {
        return $this->csvArray;
        // return array_merge($this->getCSVHeader,$this->csvArray);
    }

    public function __toString() {
        return 'note';
    }





}