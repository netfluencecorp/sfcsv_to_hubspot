<?php

namespace App\Models\Engagements;


Trait AssociationTrait {

    public function extractAssociations(&$data, $association)
    {


        if(isset($association['contactIds'][0])){
            $data[] = implode(':', $association['contactIds']);
        }else{
            $data[] = "";
        }

        if(isset($association['companyIds'][0])){
            $data[] = implode(':', $association['companyIds']);
        }else{
            $data[] = "";
        }

        if(isset($association['dealIds'][0])){
            $data[] = implode(':', $association['dealIds']);
        }else{
            $data[] = "";
        }

        if(isset($association['ownerIds'][0])){
            $data[] = implode(':', $association['ownerIds']);
        }else{
            $data[] = "";
        }
    }
}