<?php

namespace App\Models\Engagements;


class Task implements CSVInterface
{
    private $csvArray;
    use AssociationTrait;
    use EngagementTrait;

    public function __construct()
    {
        $this->csvArray[] = $this->getCSVHeader();
    }

    public function getCSVHeader()
    {
        return ['engagementid', 
                'createdAt',
                'lastUpdated', 
                'createdBy', 
                'modifiedBy', 
                'owner',
                'timestamp',
                'contactIds',
                'companyIds',
                'dealIds',
                'ownerIds',

                'body',
                'status',
                'forObjectType',
                'emailRemindAt',
                ];
    }



    public function flatten($engagement)
    {

        $data = []; 

        $this->extractObjectInfo($data, $engagement['engagement']);
        $this->extractAssociations($data, $engagement['associations']);
        $data[] = (isset($engagement['metadata']['body']))? $engagement['metadata']['body'] : '';
        $data[] = (isset($engagement['metadata']['status']))? $engagement['metadata']['status'] : '';
        $data[] = (isset($engagement['metadata']['forObjectType']))? $engagement['metadata']['forObjectType'] : '';

        if(count($engagement['metadata']['reminders']) > 1){
            var_dump($engagement);
        }
        if(count($engagement['metadata']['reminders']) > 0){
            $data[] = Date('Y-m-d H:i:s', ((int)($engagement['metadata']['reminders'][0]/1000)));
        }
            
        $this->csvArray[] = $data;
    }

    public function getData()
    {
        return $this->csvArray;
        // return array_merge($this->getCSVHeader,$this->csvArray);
    }

    public function __toString() {
        return 'task';
    }





}