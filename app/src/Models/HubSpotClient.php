<?php
namespace App\Models;

use Psr\Log\LoggerInterface;
use GuzzleHttp\Client;


/**
 * Class DealsFcactory.
 */
class HubSpotClient
{
    private $client;
    private $hapiKey;
    private $hubspotLog;
    private $logger;

    public function __construct(LoggerInterface $logger, Client $client, $hapiKey, $hubspotLog)
    {
        $this->logger = $logger;
        $this->client = $client;
        $this->hapiKey = $hapiKey;
        $this->hubspotLog = $hubspotLog;
    }


    public function getCompanies($offset=0 ,$limit=1,$properties = ['name'])
    {
        $urlParams['hapikey'] = $this->hapiKey;
        $urlParams['offset'] = $offset;
        $urlParams['limit'] = $limit;
        $urlParams['properties'] = $properties;
        // $urlParams['propertiesWithHistory'] = $properties;
        // $urlParams['includeAssociations'] = 'true';

        $urlString = $this->_generatePropertyUrl($urlParams);

        $url = "https://api.hubapi.com/companies/v2/companies/paged?{$urlString}";

        $response = $this->client->request('get', $url);
        $this->hubspotLog->log(__FUNCTION__, 'get');
        $body = $response->getBody();

        return json_decode((string)$body, TRUE);
    }

    public function getContactProperties($fromApi = NULL)
    {

        $urlParams['hapikey'] = $this->hapiKey;

        if(isset($fromApi)){
            $urlParams['hapikey'] = $fromApi;
        }
        
        $urlString = $this->_generatePropertyUrl($urlParams);

        $url = "https://api.hubapi.com/properties/v1/contacts/properties?{$urlString}";

        $response = $this->client->request('get', $url);
        $this->hubspotLog->log(__FUNCTION__, 'get');
        $body = $response->getBody();

        return json_decode((string)$body, TRUE);
    }

    public function getDealsProperties($fromApi = NULL)
    {

        $urlParams['hapikey'] = $this->hapiKey;

        if(isset($fromApi)){
            $urlParams['hapikey'] = $fromApi;
        }
        
        $urlString = $this->_generatePropertyUrl($urlParams);

        $url = "https://api.hubapi.com/properties/v1/deals/properties?{$urlString}";

        $response = $this->client->request('get', $url);
        $this->hubspotLog->log(__FUNCTION__, 'get');
        $body = $response->getBody();

        return json_decode((string)$body, TRUE);
    }

    public function associateContactToCompany($companyId, $vid)
    {
        $urlParams['hapikey'] = $this->hapiKey;
        $urlString = $this->_generatePropertyUrl($urlParams);
        $url = "https://api.hubapi.com/companies/v2/companies/{$companyId}/contacts/{$vid}?{$urlString}";
        
        $response = $this->client->request('put', $url);
        $this->hubspotLog->log(__FUNCTION__, 'put');
        $body = $response->getBody();
    }

    public function associateDealTo($object, $dealId, $id)
    {
        $object = strtoupper($object);
        if(!in_array($object, ['CONTACT', 'COMPANY']));
        $urlParams['hapikey'] = $this->hapiKey;
        $urlParams['id'] = $id;
        $urlString = $this->_generatePropertyUrl($urlParams);

        // 'deals/v1/deal/:dealId/associations/:OBJECTTYPE?id=:objectId&id=:objectId'
        $url = "https://api.hubapi.com/deals/v1/deal/{$dealId}/associations/{$object}?{$urlString}";
        $response = $this->client->request('put', $url);
        $this->hubspotLog->log(__FUNCTION__, 'put');
        $body = $response->getBody();
    }

    public function getCompanyProperties($fromApi = NULL)
    {

        $urlParams['hapikey'] = $this->hapiKey;

        if(isset($fromApi)){
            $urlParams['hapikey'] = $fromApi;
        }
        
        $urlString = $this->_generatePropertyUrl($urlParams);

        $url = "https://api.hubapi.com/properties/v1/companies/properties?{$urlString}";

        $response = $this->client->request('get', $url);
        $this->hubspotLog->log(__FUNCTION__, 'get');
        $body = $response->getBody();

        return json_decode((string)$body, TRUE);
    }

    public function saveContactProperties($updateProperty, $fromApi = NULL)
    {
        $urlParams['hapikey'] = $this->hapiKey;

        if(isset($fromApi)){
            $urlParams['hapikey'] = $fromApi;
        }

        $urlString = $this->_generatePropertyUrl($urlParams);
        $url = "https://api.hubapi.com/properties/v1/contacts/properties?{$urlString}";
        $options['json'] = $updateProperty;

        try{
            $body = $this->client->request('post', $url, $options);
            $this->hubspotLog->log(__FUNCTION__, 'post');
        }catch(\Exception $ex){
            echo $ex->getMessage();
        }

        return TRUE;
    }

    public function saveCompanyProperties($updateProperty, $fromApi = NULL)
    {
        $urlParams['hapikey'] = $this->hapiKey;

        if(isset($fromApi)){
            $urlParams['hapikey'] = $fromApi;
        }

        $urlString = $this->_generatePropertyUrl($urlParams);
        $url = "https://api.hubapi.com/properties/v1/deals/properties?{$urlString}";
        $options['json'] = $updateProperty;

        try{
            $body = $this->client->request('post', $url, $options);
            $this->hubspotLog->log(__FUNCTION__, 'post');
        }catch(\Exception $ex){
            echo $ex->getMessage();
        }

        return TRUE;
    } 

    public function saveDealProperties($updateProperty, $fromApi = NULL)
    {
        $urlParams['hapikey'] = $this->hapiKey;

        if(isset($fromApi)){
            $urlParams['hapikey'] = $fromApi;
        }

        $urlString = $this->_generatePropertyUrl($urlParams);
        $url = "https://api.hubapi.com/properties/v1/deals/properties?{$urlString}";
        $options['json'] = $updateProperty;

        try{
            $body = $this->client->request('post', $url, $options);
            $this->hubspotLog->log(__FUNCTION__, 'post');
        }catch(\Exception $ex){
            echo $ex->getMessage();
        }

        return TRUE;
    }

    public function getPropertyNames($object)
    {
        switch($object){
            case 'contacts':
                    $properties = $this->getContactProperties();
                break;
        }

        $propertyNames = [];
        foreach($properties as $property){
            $propertyNames[] = $property['name'];
        }

        return $propertyNames;
    }

    public function getAllContacts($properties, $offset = 0, $count = 100)
    {

        $urlParams['hapikey'] = $this->hapiKey;
        $urlParams['vidOffset'] = $offset;
        $urlParams['count'] = $count;
        $urlParams['property'] = $properties;
        $urlString = $this->_generatePropertyUrl($urlParams);
        // $urlString = urlencode($urlString);
        $url = "https://api.hubapi.com/contacts/v1/lists/all/contacts/all?{$urlString}";


        try{
            $response = $this->client->request('get', $url);
            $this->hubspotLog->log(__FUNCTION__, 'get');
        }catch(\Exception $ex){
            echo $ex->getMessage();
        }
        $body = $response->getBody();

        return json_decode((string)$body, TRUE);
    }

    public function getLastModifiedContacts($properties, $limit, $timeoffset, $vidoffset)
    {


        $urlParams['hapikey'] = $this->hapiKey;
        $urlParams['count'] = $limit;
        $urlParams['propertyMode'] = 'value_and_history';
        $urlParams['timeOffset'] = $timeoffset;
        $urlParams['vidOffset'] = $vidoffset;
        $urlParams['property'] = $properties;
        $urlParams['includePropertyVersions'] = true;

        // compute date
        $onedayhalf = 36*60*60;

        $urlString = $this->_generatePropertyUrl($urlParams);
        $url = "https://api.hubapi.com/contacts/v1/lists/recently_updated/contacts/recent?{$urlString}";
        $response = $this->client->request('get', $url);
        $this->hubspotLog->log(__FUNCTION__, 'get');
        $body = $response->getBody();

        return json_decode((string)$body, TRUE);

    }
    public function getLastModifiedDeals($offset, $limit = 100)
    {


        $urlParams['hapikey'] = $this->hapiKey;
        $urlParams['count'] = $limit;
        $urlParams['offset'] = $offset;
        $urlParams['includePropertyVersions'] = true;

        // compute date
        $onedayhalf = 36*60*60;
        $since = (time() - $onedayhalf) * 1000;
        $urlParams['since'] = $since;

        $urlString = $this->_generatePropertyUrl($urlParams);
        $url = "https://api.hubapi.com/deals/v1/deal/recent/modified?{$urlString}";

        $response = $this->client->request('get', $url);
        $this->hubspotLog->log(__FUNCTION__, 'get');
        $body = $response->getBody();

        return json_decode((string)$body, TRUE);

    }

    public function getLastModifiedCompanies($offset, $limit = 100)
    {


        $urlParams['hapikey'] = $this->hapiKey;
        $urlParams['count'] = $limit;
        $urlParams['offset'] = $offset;

        $urlString = $this->_generatePropertyUrl($urlParams);
        $url = "https://api.hubapi.com/companies/v2/companies/recent/modified?{$urlString}";

        $response = $this->client->request('get', $url);
        $this->hubspotLog->log(__FUNCTION__, 'get');
        $body = $response->getBody();

        return json_decode((string)$body, TRUE);

    }

    public function fetchDeal(Int $dealId)
    {

        $getDeal = "https://api.hubapi.com/deals/v1/deal/{$dealId}?properties=dealname&properties=est__monthly_signups&hapikey={$this->hapiKey}";

        $response = $this->client->request('get', $getDeal);
        $this->hubspotLog->log(__FUNCTION__, 'get');
        $body = $response->getBody();

        return json_decode((string)$body, TRUE);
    }

    

    public function getCompanyDetail(Int $companyId, $properties = [])
    {

        $urlParams['hapikey'] = $this->hapiKey;
        $urlParams['property'] = $properties;

        $urlString = $this->_generatePropertyUrl($urlParams);
        $url = "https://api.hubapi.com/companies/v2/companies/{$companyId}?{$urlString}";

        $response = $this->client->request('get', $url);
        $this->hubspotLog->log(__FUNCTION__, 'get');
        $body = $response->getBody();

        return json_decode((string)$body, TRUE);
    }

    public function getContact(Int $contactId, $properties = [])
    {

        $urlParams['hapikey'] = $this->hapiKey;
        $urlParams['property'] = $properties;

        $urlString = $this->_generatePropertyUrl($urlParams);
        $url = "https://api.hubapi.com/contacts/v1/contact/vid/{$contactId}/profile?{$urlString}";

        $response = $this->client->request('get', $url);
        $this->hubspotLog->log(__FUNCTION__, 'get');
        $body = $response->getBody();

        return json_decode((string)$body, TRUE);
    }


    public function getDeal(Int $dealId, $properties = [])
    {

        $urlParams['hapikey'] = $this->hapiKey;
        $urlParams['property'] = $properties;

        $urlString = $this->_generatePropertyUrl($urlParams);

        $url = "https://api.hubapi.com/deals/v1/deal/{$dealId}?{$urlString}";

        $response = $this->client->request('get', $url);
        $this->hubspotLog->log(__FUNCTION__, 'get');
        $body = $response->getBody();

        return json_decode((string)$body, TRUE);
    }

    public function getAllDeals($offset = 0, $limit = 50, $properties = ['dealname'])
    {
        $urlParams['hapikey'] = $this->hapiKey;
        $urlParams['properties'] = $properties;
        $urlParams['limit'] = $limit;
        $urlParams['offset'] = $offset;
        $urlParams['includeAssociations'] = 'true';

        $urlString = $this->_generatePropertyUrl($urlParams);

        $url = "https://api.hubapi.com/deals/v1/deal/paged?{$urlString}";
        echo $url;

        $response = $this->client->request('get', $url);
        $this->hubspotLog->log(__FUNCTION__, 'get');
        $body = $response->getBody();

        return json_decode((string)$body, TRUE);
    }

    public function getContactsFromCompany(Int $companyId)
    {

        $urlParams['hapikey'] = $this->hapiKey;
        $urlString = $this->_generatePropertyUrl($urlParams);
        $url = "https://api.hubapi.com/companies/v2/companies/{$companyId}/vids?{$urlString}";

        $response = $this->client->request('get', $url);
        $this->hubspotLog->log(__FUNCTION__, 'get');
        $body = $response->getBody();

        return json_decode((string)$body, TRUE);
    }

    public function syncDeals($dealIds, $updateProperty)
    {

        $updateProperty = ['properties' => $updateProperty];
        $urlParams['hapikey'] = $this->hapiKey;
        $urlString = $this->_generatePropertyUrl($urlParams);

        $tmpValues = [];

        // convert ['property'] to ['name']
        foreach($updateProperty['properties'] as $updateProperty)
        {
            if($updateProperty['property'] == 'hubspot_owner_id'){
                continue;
            }
            $tmpValues[] = ['name' => $updateProperty['property'],
                            'value' => $updateProperty['value']
                            ];
        }



        // update all deals
        foreach($dealIds as $dealId){
            $url = "https://api.hubapi.com/deals/v1/deal/{$dealId}?{$urlString}";
            $options['json'] = ['properties' => $tmpValues];
            
            try{
                $body = $this->client->request('put', $url, $options);
                $this->hubspotLog->log(__FUNCTION__, 'put');
            }catch(\Exception $ex){
                continue;
            }
        }
    }

    public function createDeal($properties, $logIdentifier = NULL)
    {
        $updateProperty = [];
        foreach($properties as $property){

            if($property['property'] == 'dealstage'){
                $updateProperty[] = [
                    'name'  => $property['property'],
                    'value' => $this->getCalReplyDealStage($property['value']),
                ];
                continue;
            }elseif($property['property'] == 'pipeline'){
                $updateProperty[] = [
                    'name'  => $property['property'],
                    'value' => '94dc8fe5-eea4-4d2f-ad37-322c7b423cd7'
                ];
                continue;
            }
            $updateProperty[] = [
                'value' => $property['value'],
                'name' => $property['property'],
                
            ];

        }
        $updateProperty = ['associations' => ['associatedCompanyIds' => [], 'associatedVids' => []], 'properties' => $updateProperty];

        $urlParams['hapikey'] = $this->hapiKey;
        $urlString = $this->_generatePropertyUrl($urlParams);


        // update all deals
        $url = "https://api.hubapi.com/deals/v1/deal?{$urlString}";
        $options['json'] = $updateProperty;

        // var_dump($options);



        try{

            $body = $this->client->request('post', $url, $options);
            $this->hubspotLog->log(__FUNCTION__, 'post');
            return json_decode($body->getBody()->getContents(), TRUE);

        }catch(\Exception $ex){
            echo $ex->getResponse()->getBody(true);
            // echo "\n";
            $this->logger->warning('CREATE DEAL problem with '. $logIdentifier);
        }
        return False;
    }

    public function createContact($updateProperty, $logIdentifier = NULL)
    {

        $updateProperty = ['properties' => $updateProperty];

        $urlParams['hapikey'] = $this->hapiKey;
        $urlString = $this->_generatePropertyUrl($urlParams);

        // update all deals
        $url = "https://api.hubapi.com/contacts/v1/contact?{$urlString}";
        $options['json'] = $updateProperty;

        try{
            $body = $this->client->request('post', $url, $options);
            $this->hubspotLog->log(__FUNCTION__, 'post');
            return json_decode($body->getBody()->getContents(), TRUE);
        }catch(\Exception $ex){
            echo $bodyMessage = $ex->getResponse()->getBody(true);
            $this->logger->warning('CREATE COMPANY problem with '. $logIdentifier . " " . $bodyMessage);
        }
        return False;
    }

    public function createCompany($properties, $logIdentifier = NULL)
    {

        $updateProperty = [];
        foreach($properties as $property){

            $updateProperty[] = [
                'name' => $property['property'],
                'value' => $property['value'],
            ];
        }

        $updateProperty = ['properties' => $updateProperty];

        $urlParams['hapikey'] = $this->hapiKey;
        $urlString = $this->_generatePropertyUrl($urlParams);

        // update all deals
        $url = "https://api.hubapi.com/companies/v2/companies?{$urlString}";
        $options['json'] = $updateProperty;

        try{
            $body = $this->client->request('post', $url, $options);
            $this->hubspotLog->log(__FUNCTION__, 'post');
            return json_decode($body->getBody()->getContents(), TRUE);
        }catch(\Exception $ex){
            echo $bodyMessage = $ex->getResponse()->getBody(true);
            $this->logger->warning('CREATE COMPANY problem with '. $logIdentifier . " " . $bodyMessage);
        }
        return False;
    }

    public function syncContacts($contactIds, $updateProperty)
    {
        
        $updateProperty = ['properties' => $updateProperty];

        $urlParams['hapikey'] = $this->hapiKey;
        $urlString = $this->_generatePropertyUrl($urlParams);

        // update all deals
        foreach($contactIds as $contactId){
            $url = "https://api.hubapi.com/contacts/v1/contact/vid/{$contactId}/profile?{$urlString}";
            $options['json'] = $updateProperty;

            try{
                $body = $this->client->request('post', $url, $options);
                $this->hubspotLog->log(__FUNCTION__, 'post');
            }catch(\Exception $ex){
                continue;
            }
        }
    }

    public function syncCompany($companyIds, $updateProperty)
    {
        $urlParams['hapikey'] = $this->hapiKey;
        $updateProperty = ['properties' => $updateProperty];

        
        $urlString = $this->_generatePropertyUrl($urlParams);
        
        // convert ['property'] to ['name']
        foreach($updateProperty['properties'] as $updateProperty)
        {

            $tmpValues[] = ['name' => $updateProperty['property'],
                            'value' => $updateProperty['value']
                            ];
        }

        // update all company
        foreach($companyIds as $companyId){
            $url = "https://api.hubapi.com/companies/v2/companies/{$companyId}?{$urlString}";
            $options['json'] = ['properties' => $tmpValues];
            try{
                $body = $this->client->request('PUT', $url, $options);
                $this->hubspotLog->log(__FUNCTION__, 'put');
            }catch(\Exception $ex){
                continue;
            }
        }
    }


    public function getAssociatedDeals(Int $id, $object)
    {

        $urlParams['hapikey'] = $this->hapiKey;
        $urlParams['includeAssociations'] = true;
        $urlParams['limit'] = 100;
        $urlString = $this->_generatePropertyUrl($urlParams);

        $url = "https://api.hubapi.com/deals/v1/deal/associated/{$object}/{$id}/paged?{$urlString}";


        $response = $this->client->request('get', $url);
        $this->hubspotLog->log(__FUNCTION__, 'get');
        $body = $response->getBody();

        return json_decode((string)$body, TRUE);
    }

    private function _generatePropertyUrl(Array $properties)
    {
        $tmpArray = [];
        foreach($properties as $key => $value){
            if(is_array($value)){
                foreach($value as $x){
                    $tmpArray[] = $key . '=' . urlencode($x);
                }
            }else{
                $tmpArray[] = $key . '=' . urlencode($value);
            }
        }

        return implode("&", $tmpArray);
    }

    public function getOwners()
    {

        $urlParams['hapikey'] = $this->hapiKey;
        $urlString = $this->_generatePropertyUrl($urlParams);
        $url = "https://api.hubapi.com/owners/v2/owners/?{$urlString}";

        $response = $this->client->request('get', $url);
        $this->hubspotLog->log(__FUNCTION__, 'get');
        $body = $response->getBody();

        return json_decode((string)$body, TRUE);

    }

    public function updatePropertyValues($object, $propertyName, $values)
    {
        try{
            $property = $this->getPropertyValue($object, $propertyName);
        }catch(\Exception $ex){
            $exceptionBody = json_decode($ex->getResponse()->getBody());
            echo "[" . date('Y-m-d H:i:s') ."] ERROR: ". $exceptionBody->message ."\nTerminating script... Done.\n";
            exit(1);
        }

        $json['label'] = $property['label'];
        $json['groupName'] = $property['groupName'];
        $json['fieldType'] = 'select';
        $json['options'] = $values;
        $json['type'] = 'enumeration';

        $urlParams['hapikey'] = $this->hapiKey;
        $urlString = $this->_generatePropertyUrl($urlParams);

        $url = "https://api.hubapi.com/properties/v1/{$object}/properties/named/{$propertyName}?{$urlString}";
        $options['json'] = $json;
        $body = $this->client->request('PUT', $url, $options);
        $this->hubspotLog->log(__FUNCTION__, 'put');
    }

    public function getPropertyValue($object, $propertyName)
    {
        $urlParams['hapikey'] = $this->hapiKey;
        $urlString = $this->_generatePropertyUrl($urlParams);

        $url = "https://api.hubapi.com/properties/v1/{$object}/properties/named/{$propertyName}?{$urlString}";

        $response = $this->client->request('get', $url);
        $this->hubspotLog->log(__FUNCTION__, 'get');
        $body = $response->getBody();

        return json_decode((string)$body, TRUE);
    }

    public function getCalReplyDealStage($dealstage)
    {
        $stageId = NULL;
        switch($dealstage){
            case "Target":
                $stageId = "7c1c4bcc-5c2b-47e3-937a-af2900c3c3f8";
            break;

            case "Initial Pitch":
                $stageId = "27dc4a74-6710-4eb3-8171-1f19ecf472a7";
            break;

            case "Proposal":
                $stageId = "3ba483dc-43b5-4fc8-8903-0675cbc51669";
            break;

            case "Evaluation":
                $stageId = "d910c181-e2c5-4757-88c0-319fc21db9ce";
            break;

            case "Contract Sent":
                $stageId = "e4b32ca7-3f3d-4366-b700-a70033d3486e";
            break;

            case "Legal/Finance/Creative":
                $stageId = "8bf4fa58-0780-4e93-9710-2a8e2f8b8e92";
            break;

            case "Contract Signed":
                $stageId = "b5db4537-d0a0-4665-8716-36cffdb6e80a";
            break;

            case "Live":
                $stageId = "76325ed0-f2f4-4a71-b0d5-78a732378ff9";
            break;

            case "Closed Lost":
                $stageId = "dd929c53-3142-4946-adf7-7683ad815077";
            break;
        }
        return $stageId;

    }

    public function getEngagements($offset=0 ,$limit=1)
    {
        $urlParams['hapikey'] = $this->hapiKey;
        if($offset > 0){
            $urlParams['offset'] = $offset;    
        }
        $urlParams['limit'] = $limit;

        $urlString = $this->_generatePropertyUrl($urlParams);
        $url = "https://api.hubapi.com/engagements/v1/engagements/paged?{$urlString}";

        $response = $this->client->request('get', $url);
        $this->hubspotLog->log(__FUNCTION__, 'get');
        $body = $response->getBody();

        return json_decode((string)$body, TRUE);
    }


}