<?php

namespace App\Models;

// use App\Models\HubSpotClient;
use Psr\Log\LoggerInterface;
use PDO;

/**
 * Class PoundifModel.
 */
class PoundfitModel
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \PDO
     */
    private $pdo;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     * @param \PDO                     $pdo
     */
    public function __construct(LoggerInterface $logger, PDO $pdo)
    {
        $this->logger = $logger;
        $this->pdo = $pdo;
    }

    public function getNolatlong()
    {
        $sql = "SELECT * FROM accounts where shipping_country IS NOT NULL AND (shipping_latitude IS NULL OR shipping_longitude IS NULL) LIMIT 30";
        
        try {

            $statement = $this->pdo->prepare($sql);
            $statement->execute();
  
        } catch(PDOException $e) {
            echo $e->getMessage();
            exit;
        }

        if($statement->rowCount() == 0){
            return FALSE;
        }

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    
    public function saveLatLong($id, $lat, $long)
    {
        $sql = "UPDATE `accounts` 
                    SET `shipping_latitude` = '{$lat}'
                    AND `shipping_longitude` = '{$long}' 
                WHERE `id` = {$id}";

        try {
            $statement = $this->pdo->prepare($sql);
            $statement->execute();
        } catch(PDOException $e) {
            echo $e->getMessage();
            exit;
        }

        return $statement;
    }
}