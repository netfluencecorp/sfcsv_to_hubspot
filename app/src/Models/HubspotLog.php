<?php

namespace App\Models;

use PDO;

class HubspotLog
{

    /**
     * @var \PDO
     */
    private $pdo;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     * @param \PDO                     $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
        /**
        *
        CREATE TABLE `logs`(
            `id` INT(11) PRIMARY KEY AUTO_INCREMENT,
            `object` VARCHAR(255) NOT NULL,
            `type` VARCHAR(255) NULL,
            `executed_at` DATETIME
        )
        */
    }

    public function log($object, $type = NULL)
    {
        $sql = "INSERT INTO `logs` VALUES(NULL, ?, ?, NOW())";
        $statement = $this->pdo->prepare($sql);

        return $statement->execute([$object, $type]); 
    }
}
