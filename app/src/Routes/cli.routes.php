<?php
if (!isset($argv)) {
    return;
}

use App\Action\Export;
use App\Action\Import;


// use: php public/index.php /export-contacts GET x=x
$app->get('/export-contacts', Export::class.':exportContacts');

// use: php public/index.php /import-contacts GET path=/path/to/file
$app->get('/import-contacts', Import::class.':uploadContactCSV');

// use: php public/index.php /export-owners GET path=/path/to/file
$app->get('/export-owners', Export::class.':exportOwners');

// use: php public/index.php /export-companies GET path=/path/to/file
$app->get('/export-companies', Export::class.':getAllCompanies');

// use: php public/index.php /export-deals GET path=/path/to/file
$app->get('/export-deals', Export::class.':getAllDeals');

// use: php public/index.php /import-companies GET path=/path/to/file
$app->get('/import-companies', Import::class.':uploadCompanyCSV');

// use: php public/index.php /import-deals GET path=/path/to/file
$app->get('/import-deals', Import::class.':uploadDealCSV');

// use: php public/index.php /associate-companies-contacts GET x=x
$app->get('/associate-companies-contacts', Import::class.':associateContactsCompany');

// use: php public/index.php /associate-deal-company GET x=x
$app->get('/associate-deal-company', Import::class.':associateDealsCompany');

// use: php public/index.php /transfer-contact-property GET "fromapi=8935d20f-e704-4af6-8f1f-cc202c221180&toapi=8f4c8efd-4449-42e6-a22a-e38191696be7"
$app->get('/transfer-contact-property', Export::class.':transferContactProperty');

// use: php public/index.php /transfer-company-property GET "fromapi=8935d20f-e704-4af6-8f1f-cc202c221180&toapi=8f4c8efd-4449-42e6-a22a-e38191696be7"
$app->get('/transfer-company-property', Export::class.':transferCompanyProperty');

// use: php public/index.php /transfer-deal-property GET "fromapi=8935d20f-e704-4af6-8f1f-cc202c221180&toapi=8f4c8efd-4449-42e6-a22a-e38191696be7"
$app->get('/transfer-deal-property', Export::class.':transferDealProperty');

// use: php public/index.php /transfer-contact-property GET count=30000
$app->get('/alexa', Export::class.':alexa');

// use: php public/index.php /poundfit-fix GET x=x
$app->get('/poundfit-fix', Export::class.':getNolatlong');

// use: php public/index.php /extract-engagements GET x=x
$app->get('/extract-engagements', Export::class.':getAllEngagements');

