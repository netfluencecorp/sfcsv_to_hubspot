<?php

namespace App\Action;

use Psr\Log\LoggerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use App\Models\Deals;
use App\Models\Engagements;
use Nticaric\Awis\Awis;
/**
 * Class DealsAction.
 */
final class Export
{

    private $logger;
    private $poundfitModel;
    private $dealsModel;
    private $hubspotClient;

    public function __construct(LoggerInterface $logger, $dealsModel, $hubspotClient, $poundfitModel)
    {
        $this->logger = $logger;
        $this->dealsModel = $dealsModel;
        $this->poundfitModel = $poundfitModel;
        $this->hubspotClient = $hubspotClient; // example of injecting 

    }

    public function getAllEngagements(Request $request, Response $response, $args)
    {
        $call = new Engagements\Call();
        $meeting = new Engagements\Meeting();
        $note = new Engagements\Note();
        $email = new Engagements\Email();
        $task = new Engagements\Task();

        $limit       = 250;
        $offset      = 34899637; // meeting
        $offset      = 16383887; // email
        // $offset      = 185617384; // publishing task
        // $offset      = 0;
        $engagements = $this->hubspotClient->getEngagements($offset, $limit);
        $oneDay      = 32*60*60;
        $last24hrs   = time() - $oneDay;
        $loopMore    = TRUE;
        $count       = 1;
        $syncCount   = 0;

        while($loopMore)
        {
            

            foreach($engagements['results'] as $engagement){
                $count++;

                $engagementType = strtolower($engagement['engagement']['type']);

                if(in_array(strtolower($engagement['engagement']['type']), ['call','note','email','meeting','task'])){
                    $$engagementType->flatten($engagement);
                }else{
                    switch($engagementType) {
                        case 'forwarded_email':
                            $email->flatten($engagement);
                            break;
                        case 'publishing_task':
                            // $task->flatten($engagement);
                            break;
                    }
                }
            }
        

            // $this->_exportEngagementData($email);
            // exit;

            if($engagements['hasMore']){
                $loopMore = TRUE;
                $offset = $engagements['offset'];
                $engagements = $this->hubspotClient->getEngagements($offset+1, $limit);
            }else{
                $loopMore = FALSE;
            }
        }
        echo "TOTAL ENGAGEMENT = {$count}";

        $this->_exportEngagementData($call);
        $this->_exportEngagementData($meeting);
        $this->_exportEngagementData($note);
        $this->_exportEngagementData($email);
        $this->_exportEngagementData($task);
     
    }

    private function _exportEngagementData($object)
    {

        $csvArray = $object->getData();
        // print_r($csvArray);exit;
        $fp = fopen('./'.$object.'.csv', 'w+');
        foreach ($csvArray as $fields) {
            // var_dump($fields);exit;
            fputcsv($fp, $fields);
            // exit;
        }
        fclose($fp);
    }


    public function __invoke(Request $request, Response $response, $args)
    {
        $response->write('Home Page');
        return $response;
    }

    public function transferContactProperty(Request $request, Response $response, $args)
    {

        $fromapi = $request->getQueryParams()['fromapi'];
        $toapi = $request->getQueryParams()['toapi'];    
        $properties = $this->hubspotClient->getContactProperties($fromapi);

        foreach($properties as $property) {
            $this->hubspotClient->saveContactProperties($property, $toapi);    
        }
    }

    public function transferDealProperty(Request $request, Response $response, $args)
    {

        $fromapi = $request->getQueryParams()['fromapi'];
        $toapi = $request->getQueryParams()['toapi'];    
        $properties = $this->hubspotClient->getDealsProperties($fromapi);

        foreach($properties as $property) {
            $this->hubspotClient->saveDealProperties($property, $toapi);    
        }
    }

    public function transferCompanyProperty(Request $request, Response $response, $args)
    {

        $fromapi = $request->getQueryParams()['fromapi'];
        $toapi = $request->getQueryParams()['toapi'];    
        $properties = $this->hubspotClient->getCompanyProperties($fromapi);
        foreach($properties as $property) {
            $this->hubspotClient->saveCompanyProperties($property, $toapi);    
        }
    }

    public function getAllCompanies(Request $request, Response $response, $args)
    {

        $queryParams = $request->getQueryParams();
        if(!isset($queryParams['path']) && empty($queryParams['path'])){
            echo "There are no path parameter\n";
            echo "Usage: php public/index.php export-companies GET path=/file/path\n";
            return;
        }

        $offset = 0;
        $limit = 250;
        $properties = ['name', 'website'];
        $companies = $this->hubspotClient->getCompanies($offset, $limit, $properties);
        $loopMore = TRUE;
        $count = 1;
        $fixCount = 0;

        $data[] = ['id', 'name', 'website'];

        while($loopMore)
        {

            foreach($companies['companies'] as $company){
                $tmp = [];

                $tmp[] = $company['companyId'];
                foreach($company['properties'] as $property){
                    $tmp[] = $property['value'];
                }
                $data[] = $tmp;
            }


            if($companies['has-more']){
                $loopMore = TRUE;
                $offset = $companies['offset'];
                $companies = $this->hubspotClient->getCompanies($offset, $limit, $properties);
            }else{
                $loopMore = FALSE;
            }

        }

        $fp = fopen($queryParams['path'], 'w+');
        foreach ($data as $fields) {
            fputcsv($fp, $fields);
        }
        fclose($fp);

    }


    public function getAllDeals(Request $request, Response $response, $args)
    {

        $queryParams = $request->getQueryParams();
        if(!isset($queryParams['path']) && empty($queryParams['path'])){
            echo "There are no path parameter\n";
            echo "Usage: php public/index.php export-deals GET path=/file/path\n";
            return;
        }

        $offset = 0;
        $limit = 250;
        $properties = ['dealname', 'hubspot_owner_id'];
        $deals = $this->hubspotClient->getAllDeals($offset, $limit, $properties);
        $loopMore = TRUE;
        $count = 1;
        $fixCount = 0;

        $data[] = ['id', 'dealname', 'hubspotowner', 'contacts', 'companies'];

        while($loopMore)
        {

            foreach($deals['deals'] as $deal){

                $tmp = [];
                $tmp[] = $deal['dealId'];

                foreach($properties as $property){
                    if(isset($deal['properties'][$property]['value'])){
                        $tmp[] = $deal['properties'][$property]['value'];
                    }else{
                        $tmp[] = '';
                    }
                }

                if(count($deal['associations']['associatedVids']) > 0){
                    $tmp[] = implode(":", $deal['associations']['associatedVids']);
                }else{
                    $tmp[] = '';
                }
                if(count($deal['associations']['associatedCompanyIds']) > 0){
                    $tmp[] = implode(":", $deal['associations']['associatedCompanyIds']);
                }else{
                    $tmp[] = '';
                }
                $data[] = $tmp;
            }

            if($deals['hasMore']){
                $loopMore = TRUE;
                $offset = $deals['offset'];
                $deals = $this->hubspotClient->getAllDeals($offset, $limit, $properties);
            }else{
                $loopMore = FALSE;
            }
        }

        $fp = fopen($queryParams['path'], 'w+');
        foreach ($data as $fields) {
            fputcsv($fp, $fields);
        }
        fclose($fp);
        exit;

    }

    public function alexa(Request $request, Response $response, $args)
    {


    
        if(!isset($request->getQueryParams()['count']) 
        ){
            echo "There are no Count parameter\n";
            exit;
        }

        $requestedCount = (int) $request->getQueryParams()['count'];

        $secret = 'rjyOXOtZmpS7iOQUtA30u/yAYjry6PTqak9OYiFq';
        $key = 'AKIAI2U6IJBK72GDTQMQ';

        $url      = "axs.com";
        $awis     = new Awis($key, $secret);
        $sitecount = 1;

        echo "count,sitetitle,sitename,rank,";
        echo "pageviewpermil_sum,";
        echo "pageviewpermil_avg,";
        // echo "pageviewperuser_sum,";
        echo "pageviewperuser_avg,";
        echo "reachpermil_sum,";
        echo "reachpermil_avg,\n";

        while($sitecount <= $requestedCount)
        {
            try{
                // $response = $awis->getUrlInfo($url, "UsageStats");
                // $response = $awis->getCategoryBrowse("example.com", "Categories", "Top/Shopping");

                //prints the raw xml response
                // $response = $awis->getCategoryListings("example.com", "Top/Shopping", "Popularity", "False", 5, 20);
                $response = $awis->getCategoryListings("example.com", "Top/Shopping", "Popularity", 'True', $sitecount, 20);
            }catch(\Exception $ex){
                echo $ex->getMessage();
            }
           
            $xmlString = (string) $response->getBody();
            // $xmlString = $this->xmlData;


            // get site url
            $tmp = [];
            preg_match_all(
                '/<aws:DataUrl type="navigable">(.+?)<\/aws:DataUrl>/',
                $xmlString,
                $tmp,
                PREG_PATTERN_ORDER
            );  
            $siteNames = $tmp[1];

            // get title
            $tmp = [];
            preg_match_all(
                '/<aws:Title>(.+?)<\/aws:Title>/',
                $xmlString,
                $tmp,
                PREG_PATTERN_ORDER
            );  
            $titles = $tmp[1];

            // get rank
            $tmp = [];
            preg_match_all(
                '/<aws:PopularityRank>(.+?)<\/aws:PopularityRank>/',
                $xmlString,
                $tmp,
                PREG_PATTERN_ORDER
            );
            $ranks = $tmp[1];

            $count = 0;



            foreach($siteNames as $siteName) {
                echo "\"{$sitecount}\",";
                echo "\"".html_entity_decode($titles[$count])."\",";
                echo "\"{$siteName}\",";
                echo "\"{$ranks[$count]}\",";


                $newDate = strtotime(date('Y-m-d').' - 31 days');
                $response = $awis->getTrafficHistory($siteName, 31, date('Ymd',$newDate));
                $traffic = (string) $response->getBody();
                $traffic = preg_replace('/[\r\n]+/', "\n", $traffic);
                $traffic = preg_replace('/[ \t]+/', ' ', $traffic);   
                
                $traffic = trim(preg_replace('/\s+/', ' ', $traffic));            
                $traffic = str_replace('> <',"><",$traffic);

                // page views per million
                $tmp = [];
                preg_match_all(
                    '/<aws:PerUser>(.+?)<\/aws:PerUser>/',
                    $traffic,
                    $tmp,
                    PREG_PATTERN_ORDER
                );
                $pageviewperuser = $tmp[1];

                $tmp = [];
                preg_match_all(
                    '/<aws:PageViews><aws:PerMillion>(.+?)<\/aws:PerMillion><aws:PerUser>/',
                    $traffic,
                    $tmp,
                    PREG_PATTERN_ORDER
                );
                $pageviewpermil = $tmp[1];

                $tmp = [];
                preg_match_all(
                    '/<aws:Reach><aws:PerMillion>(.+?)<\/aws:PerMillion><\/aws:Reach>/',
                    $traffic,
                    $tmp,
                    PREG_PATTERN_ORDER
                );
                $reachpermil = $tmp[1];
                $pageviewpermil_sum = array_sum($pageviewpermil);
                $pageviewpermil_avg = array_sum($pageviewpermil) / count($pageviewpermil); 

                $pageviewperuser_sum = array_sum($pageviewperuser);
                $pageviewperuser_avg = array_sum($pageviewperuser) / count($pageviewperuser);

                $reachpermil_sum = array_sum($reachpermil);
                $reachpermil_avg = array_sum($reachpermil) / count($reachpermil);

                echo "\"{$pageviewpermil_sum}\",";
                echo "\"{$pageviewpermil_avg}\",";
                // echo "\"{$pageviewperuser_sum}\",";
                echo "\"{$pageviewperuser_avg}\",";
                echo "\"{$reachpermil_sum}\",";
                echo "\"{$reachpermil_avg}\",\n";

                
                if($sitecount >= $requestedCount){
                    return;
                }
                $sitecount++;
                $count ++;
            }

        } // end of while(sitecount)
    }

    public function exportOwners(Request $request, Response $response, $args)
    {
        $owners = $this->hubspotClient->getOwners();

        $queryParams = $request->getQueryParams();
        if(!isset($queryParams['path']) && empty($queryParams['path'])){
            echo "There are no property parameter\n";
            echo "Usage: php public/index.php export-owners GET path=/file/path\n";
            return;
        }

        $data = [];
        // $blacklistId = [14082807,14082702,16392361,18438044,15383335,15383350,15383355,15383310,15726946,19798907,15383340,17488056,17288926,14687080];

        $data[] = ['ownerId','firstName','lastName','email','createdAt','updatedAt','remoteList','remoteType'];
        
        foreach($owners as $owner)
        {
            
            $tmp = [];
            $tmp[] = (isset($owner['ownerId']))? $owner['ownerId'] : '';
            $tmp[] = (isset($owner['firstName']))? $owner['firstName'] : '';
            $tmp[] = (isset($owner['lastName']))? $owner['lastName'] : '';
            $tmp[] = (isset($owner['email']))? $owner['email'] : '';
            $tmp[] = (isset($owner['createdAt']))?  Date('Y-m-d H:i:s', ((int)($owner['createdAt']/1000))) : '';
            $tmp[] = (isset($owner['updatedAt']))?  Date('Y-m-d H:i:s', ((int)($owner['updatedAt']/1000))) : '';

            if(isset($owner['remoteList']) && count($owner['remoteList']) > 0){
                foreach($owner['remoteList'] as $remote){
                    $tmp[] = $remote['remoteId'];
                    $tmp[] = $remote['remoteType'];
                }
            }
            $data[] = $tmp;
        }

        // print_r($csvArray);exit;
        $fp = fopen($queryParams['path'], 'w+');
        foreach ($data as $fields) {
            fputcsv($fp, $fields);
        }
        fclose($fp);
    }

    public function exportContacts(Request $request, Response $response, $args)
    {
        $contactPropertiesNames = $this->hubspotClient->getPropertyNames('contacts');
        $chunk = array_chunk($contactPropertiesNames, (count($contactPropertiesNames) / 2));
        $limit = 100;


        $contactPropertiesNames = ['firstname','lastname','email','mobilephone'];

        // header 
        echo '"vid",';
        foreach($contactPropertiesNames as $name){
            echo "\"{$name}\",";
        }
        echo "\n";
        
        $chunk = [['firstname','lastname','email','mobilephone']];
        

        $contacts = $this->hubspotClient->getAllContacts($chunk[0], 0, $limit);
        $contactLoop = 1;



        while($contacts['has-more'])
        {
            foreach($contacts['contacts'] as $contact)
            {
                $contactProperties = $contact['properties'];
                $vid = $contact['vid'];
                echo "\"{$vid}\",";

                if(count($chunk) > 0){
                    for($x = 1; $x < count($chunk); $x++){
                        $chunkContactProperty = $this->hubspotClient->getContact($vid, $chunk[$x]);    
                        $contactProperties = array_merge($contactProperties, $chunkContactProperty['properties']);  
                    }
                }


                foreach($contactPropertiesNames as $name)
                {
                    if(!isset($contactProperties[$name])){
                        echo "\"\",";
                    }else{
                        $str = str_replace('"', '\"', $contactProperties[$name]['value']);
                        echo "\"{$str}\",";
                    }
                }
                echo "\n";
            }

            $contacts = $this->hubspotClient->getAllContacts($chunk[0], $contacts['vid-offset'], $limit);
        }
    }



    public function uploadContactCSV(Request $request, Response $response, $args)
    {

        $fileName = $request->getQueryParams()['path'];
        $firstRow = True;
        
        if (($handle = fopen($fileName, "r")) !== FALSE) {
            while (($rowData = fgetcsv($handle, 15000, ",")) !== FALSE) {
                if($firstRow){
                    $columnNames = $rowData;
                    $firstRow = FALSE;
                    // $model->createTable($tableName, $columnNames);
                    continue;
                }

                $tmp = array_combine($columnNames, $rowData);
                
                var_dump($tmp);
                echo "\n-----\n";


                $result = $model->uploadData($tableName, $tmp);

                // $date = date('Y-m-d H:i');
                // if($result->getAffectedRows() > 0){
                //     // echo "\033[0m[{$rowCount}][{$date}]\033[32m {$tmp['Id']} inserted to {$tableName}  \n";
                //     $successCount++;
                // }else{
                //     // echo "\033[0m[{$rowCount}][{$date}]\033[31m insert FAILED to {$tableName} \n";
                //     $errorCount++;
                // }

                $rowCount++;
            }
            fclose($handle);
        }


    }

    public function getDeals(Request $request, Response $response, $args)
    {

        $this->logger->info("Chrome Deals '/' route");
        $queryParams = $request->getQueryParams();

        // Get Body
        $body = $request->getParsedBody();

        if(!isset($body['dealIds']) && count($body['dealIds']) == 0){
            return $response
                ->withJson(
                    ['result' => false, 
                    'message'=>'dealsIds are missing in request body'])
                ->withStatus(501);;
        }
        $dealIds = $body['dealIds'];

        $result = $this->dealsModel->getDeals($dealIds);

        // example
        foreach($result as $row){

            $responseArray[] = [
                                'dealId' => (int)$row['dealId'],
                                'dealName' => $row['dealName']
                                ];
        }

        return $response->withJson(['result' => true, 'deals' => $responseArray])
                        ->withHeader('Content-type', 'application/json')
                        ->withHeader('Access-Control-Allow-Origin', '*')
                        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
                        ->withHeader('Access-Control-Allow-Methods', 'POST, PUT, DELETE, OPTIONS')
                        ->withStatus(200);
    }



   
    public function getNolatlong()
    {

        // header('Content-type: text/csv');
        // header('Content-disposition: attachment;filename=FixedAccounts.Poundfit.csv');
        $result = $this->poundfitModel->getNolatlong();

        $firstRow = true;
        foreach($result as $row){



            $address = '';
            if(!empty($row['shipping_street'])){
                $address .= $row['shipping_street'] . " ";
            }


            if(!empty($row['shipping_city'])){
                $address .= $row['shipping_city'] . " ";
            }
            

            if(!empty($row['shipping_state'])){
                $address .= $row['shipping_state'] . " ";
            }
            

            if(!empty($row['shipping_country'])){
                $address .= $row['shipping_country'] . " ";
            }
            

            $location = $this->getAddressFromGmaps($address);


            $id = $row['id'];
            $this->poundfitModel->saveLatLong($id, $location['lat'], $location['lon']);

            if($firstRow){
                echo implode(",",array_keys($row));
                $firstRow = false;
                echo "\n";
            }
            foreach($row as $key => $value){
                // if(in_array($key, ['shipping_']))
                if($key == 'shipping_latitude' && !empty($location )){
                    $value = $location['lat'];
                }else if($key == 'shipping_longitude' && !empty($location )){
                    $value = $location['lon'];
                }
                $str = str_replace('"', "\"", $value);
                echo "\"{$str}\",";
            }
            echo "\n"; 


        }
    }


    public function getAddressFromGmaps($address)
    {
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($address);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // get the result of http query
        $output = curl_exec($ch);
        $location = json_decode($output, true);
        curl_close($ch);
        
        $data = NULL;
        if(isset($location['results'][0]['geometry']['location']['lat']) &&
            isset($location['results'][0]['geometry']['location']['lng']))
        {
            $data['lat'] = $location['results'][0]['geometry']['location']['lat'];
            $data['lon'] = $location['results'][0]['geometry']['location']['lng'];
        }
        return $data;
    }

}
