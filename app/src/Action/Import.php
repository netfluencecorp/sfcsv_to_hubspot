<?php

namespace App\Action;

use Psr\Log\LoggerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use App\Models\Deals;

/**
 * Class DealsAction.
 */
final class Import
{

    private $logger;
    private $accountsModel;
    private $dealsModel;
    private $companiesModel;
    private $hubspotClient;

    public function __construct(LoggerInterface $logger, $accountsModel, $dealsModel, $companiesModel, $hubspotClient)
    {
        $this->logger = $logger;
        $this->accountsModel = $accountsModel;
        $this->companiesModel = $companiesModel;
        $this->dealsModel = $dealsModel;
        $this->hubspotClient = $hubspotClient; // example of injecting 

    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $response->write('Home Page');
        return $response;
    }

    public function prepareProperties($properties)
    {
        $data = [];

        $blackListKeys = ['notes_last_updated','salesforcecontactid','salesforceaccountid', 'display_order',  'is_won',  'is_closed', 'lastmodifieddate','hs_lastmodifieddate'];
        foreach($properties as $key => $value)
        {

            if(in_array($key, $blackListKeys)){
                continue;
            }

            $tmpValue = $value;
            if((strpos( strtolower($key), 'date')) > 0 &&
                !empty($value))
            {
                     
                $date = new \DateTime( $value );

                $date->setTimezone( timezone_open( 'UTC' ) );
                $date->modify( 'midnight' );

                $tmpValue = $date->getTimestamp() * 1000;
            }

            if(!empty($value)){
                $data[] = [
                    'property'  => $key,
                    'value' => $tmpValue
                ];
            }
        }
        // exit;
        return $data;

    }

    public function uploadCompanyCSV(Request $request, Response $response, $args)
    {

        $fileName = $request->getQueryParams()['path'];
        $firstRow = True;
        $rowCount = 1;
        $errorCount = 0;
        
        if (($handle = fopen($fileName, "r")) !== FALSE) {
            while (($rowData = fgetcsv($handle, 15000, ",")) !== FALSE) {
                if($firstRow){
                    $columnNames = $rowData;
                    $firstRow = FALSE;
                    // $model->createTable($tableName, $columnNames);
                    continue;
                }

                $hubspotSent = $databaseSaved = FALSE;
                $tmp = array_combine($columnNames, $rowData);
                $propertyArray = $this->prepareProperties($tmp);

                $data = $this->hubspotClient->createCompany($propertyArray, $tmp['salesforceaccountid']);

                if($data !== FALSE)
                {
                    $tmp['companyId'] = $data['companyId'];

                    $hubspotSent = TRUE;
                    $result = $this->companiesModel->saveRow($tmp);
                    if($result->rowCount() > 0){
                        $databaseSaved = true;
                    }
                }

                $date = date('Y-m-d H:i');
                if($hubspotSent && $databaseSaved){
                    echo "\033[0m[{$rowCount}][{$date}]\033[32m inserted to companies \n";
                }else{
                    echo "\033[0m[{$rowCount}][{$date}]\033[31m insert FAILED to companies\n";
                }

                $rowCount++;
            }
            fclose($handle);
        }
    }
    public function uploadDealCSV(Request $request, Response $response, $args)
    {

        $fileName = $request->getQueryParams()['path'];
        $firstRow = True;
        $rowCount = 1;
        $errorCount = 0;
        
        if (($handle = fopen($fileName, "r")) !== FALSE) {
            while (($rowData = fgetcsv($handle, 15000, ",")) !== FALSE) {
                if($firstRow){
                    $columnNames = $rowData;
                    $firstRow = FALSE;
                    // $model->createTable($tableName, $columnNames);
                    continue;
                }

                $hubspotSent = $databaseSaved = FALSE;
                $tmp = array_combine($columnNames, $rowData);
                $propertyArray = $this->prepareProperties($tmp);



                $data = $this->hubspotClient->createDeal($propertyArray, $tmp['hs_salesforceopportunityid']);
                // var_dump($propertyArray);
                // continue;
                if($data !== FALSE)
                {
                    $tmp['dealId'] = $data['dealId'];

                    $hubspotSent = TRUE;
                    $result = $this->dealsModel->saveRow($tmp);
                    if($result->rowCount() > 0)
                    {
                        $databaseSaved = true;
                    }
                }

                $date = date('Y-m-d H:i');
                if($hubspotSent && $databaseSaved){
                    echo "\033[0m[{$rowCount}][{$date}]\033[32m inserted to deals \n";
                }else{
                    echo "\033[0m[{$rowCount}][{$date}]\033[31m insert FAILED to deals\n";
                }

                $rowCount++;
                // exit;
            }
            fclose($handle);
        }
    }


    public function uploadContactCSV(Request $request, Response $response, $args)
    {



        $fileName = $request->getQueryParams()['path'];
        $firstRow = True;
        $rowCount = 1;
        $errorCount = 0;
        
        if (($handle = fopen($fileName, "r")) !== FALSE) {
            while (($rowData = fgetcsv($handle, 15000, ",")) !== FALSE) {
                if($firstRow){
                    $columnNames = $rowData;
                    $firstRow = FALSE;
                    // $model->createTable($tableName, $columnNames);
                    continue;
                }

                $hubspotSent = $databaseSaved = FALSE;
                $tmp = array_combine($columnNames, $rowData);
                $propertyArray = $this->prepareProperties($tmp);
                
                $data = $this->hubspotClient->createContact($propertyArray, $tmp['email']);
                


                if($data !== FALSE)
                {
                    $tmp['vid'] = $data['vid'];

                    $hubspotSent = TRUE;
                    $result = $this->accountsModel->saveRow($tmp);
                    if($result->rowCount() > 0){
                        $databaseSaved = true;
                    }
                }

                $date = date('Y-m-d H:i');
                if($hubspotSent && $databaseSaved){
                    echo "\033[0m[{$rowCount}][{$date}]\033[32m inserted to contacts \n";
                }else{
                    echo "\033[0m[{$rowCount}][{$date}]\033[31m insert FAILED to contacts\n";
                }

                $rowCount++;
            }
            fclose($handle);
        }
    }

    public function associateContactsCompany(Request $request, Response $response, $args)
    {
        $result = $this->companiesModel->associateContactsFromDb();

        $rowCount = 1;
        foreach($result as $row){
            $date = date('Y-m-d H:i');
            echo "\033[0m[{$rowCount}][{$date}] \033[31m{$row['companyName']} \033[0mto\033 \033[32m {$row['email']} \033 \n";
            $this->hubspotClient->associateContactToCompany($row['companyId'], $row['vid']);
            $rowCount++;
        }


    }

    public function associateDealsCompany(Request $request, Response $response, $args)
    {
        $result = $this->dealsModel->associateDealsFromDb();

        $rowCount = 1;
        foreach($result as $row){
            $date = date('Y-m-d H:i');
            echo "\033[0m[{$rowCount}][{$date}] \033[31m{$row['companyName']} \033[0mto\033 \033[32m {$row['dealname']} \033 \n";
            $this->hubspotClient->associateDealTo('COMPANY', $row['dealId'], $row['companyId']);
            $rowCount++;
        }

        


    }


}