<?php

return [
    'settings' => [
        // Slim Settings
        'determineRouteBeforeAppMiddleware' => false,
        'displayErrorDetails' => false,
        'hapiKey' => '2c11b5b4-1eb7-4d7c-92fc-95f74fba19d8',
        'outputBuffering' => false,

        // database settings
        'pdo' => [
            'dsn' => 'mysql:host=localhost;dbname=chrome_ext;charset=utf8',
            'username' => 'chrome_ext',
            'password' => 'S46*u*5Dsh',
        ],

        // monolog settings
        'logger' => [
            'name' => 'app',
            'path' => __DIR__.'/../../../log/app.log',
        ],
    ],
];
