<?php

return [
    'settings' => [
        // Slim Settings
        'determineRouteBeforeAppMiddleware' => false,
        'displayErrorDetails' => true,
        'hapiKey' => '859d3d6d-b3fc-4ea9-bc71-1f611bf86e9e',
        'outputBuffering' => false,

        // database settings
        'pdo' => [
            'dsn' => 'mysql:host=localhost;dbname=salesforce;charset=utf8',
            'username' => 'root',
            'password' => 'password',
        ],

        // monolog settings
        'logger' => [
            'name' => 'app',
            'path' => __DIR__.'/../../../log/app.log',
        ],
    ],
];
